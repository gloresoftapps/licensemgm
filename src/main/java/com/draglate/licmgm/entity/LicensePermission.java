package com.draglate.licmgm.entity;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;


@Entity
@Table(name="LICENSE_PERMISSION")
public class LicensePermission {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="LICENSE_ID")
	@JsonBackReference
	private License licenseObject;
		
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinColumn(name = "PERMISSION_ID")
	private Permission permissionObject;
	
	@Column(name = "VALUE")
	private String value;
	
	@Column(name = "LICENSE_ID", insertable=false, updatable=false)
	private String licenseId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Permission getPermissionObject() {
		return permissionObject;
	}

	public void setPermissionObject(Permission permissionObject) {
		this.permissionObject = permissionObject;
	}

	public License getLicenseObject() {
		return licenseObject;
	}

	public void setLicenseObject(License licenseObject) {
		this.licenseObject = licenseObject;
	}

	public String getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}

}
