package com.draglate.licmgm.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;


@Entity
@Table(name="LICENSE")

public class License {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long licenseId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CUSTOMER_ID")
	private Customer customerObject;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ENVIRONMENT_ID")
	private Environment environmentObject;
	
	@Column(name = "PRICE")
	private Double price;
	
	@Column(name = "CURRENCY")
	private String currency;
	
	@Column(name = "VALIDITY")
	private Date validity;
	
	@OneToMany(cascade = CascadeType.ALL,  mappedBy = "licenseObject", orphanRemoval=true, fetch=FetchType.EAGER)
	@JsonManagedReference
	private List<LicensePermission> listOfLicensePermission;
	
			
	public List<LicensePermission> getListOfLicensePermission() {
		return listOfLicensePermission;
	}

	public void setListOfLicensePermission(List<LicensePermission> listOfLicensePermission) {
		this.listOfLicensePermission = listOfLicensePermission;
	}

	public Long getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getValidity() {
		return validity;
	}


	public void setValidity(Date validity) {
		this.validity = validity;
	}

	public Customer getCustomerObject() {
		return customerObject;
	}

	public void setCustomerObject(Customer customerObject) {
		this.customerObject = customerObject;
	}

	public Environment getEnvironmentObject() {
		return environmentObject;
	}

	public void setEnvironmentObject(Environment environmentObject) {
		this.environmentObject = environmentObject;
	}

}
