package com.draglate.licmgm.BO.license;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.draglate.licmgm.entity.LicensePermission;
/**
 * Business Object for License Management
 * @author Abhijit Ingle
 *
 */
@XmlRootElement
public class LicenseOverviewBO {
	
	private Long licenseId;
	
	private String customerName;
	
	private String environmentName;
	
	private String validity;
	
	private String price;
	
	private String currency;
	
	private List<LicensePermission> listOfPermission;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEnvironmentName() {
		return environmentName;
	}

	public void setEnvironmentName(String environmentName) {
		this.environmentName = environmentName;
	}

	

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public List<LicensePermission> getListOfPermission() {
		return listOfPermission;
	}

	public void setListOfPermission(List<LicensePermission> listOfPermission) {
		this.listOfPermission = listOfPermission;
	}

	public Long getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	

}
