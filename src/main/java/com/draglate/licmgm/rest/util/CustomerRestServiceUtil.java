package com.draglate.licmgm.rest.util;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.draglate.licmgm.BO.license.LicenseOverviewBO;
import com.draglate.licmgm.entity.*;


public class CustomerRestServiceUtil {
	
	private final static Logger LOGGER = Logger.getLogger(CustomerRestServiceUtil.class.getName());

	/**
	 * Method to convert license object to overview business object
	 * @param overviewBO: result Object
	 * @param license: input Object
	 */
	public void getLicenseOverviewBO(LicenseOverviewBO overviewBO,
			License license) {		
		    LOGGER.log(Level.INFO, "CustomerRestServiceUtil.getLicenseOverviewBO --- Inside -----");
			overviewBO.setCustomerName(license.getCustomerObject().getName());
			overviewBO.setEnvironmentName(license.getEnvironmentObject().getName());
			overviewBO.setValidity(formatDate(license.getValidity()));
			overviewBO.setPrice(license.getPrice().toString());	
			overviewBO.setLicenseId(license.getLicenseId());
			overviewBO.setListOfPermission(license.getListOfLicensePermission());
			overviewBO.setCurrency(license.getCurrency());
			LOGGER.log(Level.INFO, "CustomerRestServiceUtil.getLicenseOverviewBO --- Exit -----");
	}

	private String formatDate(Date validity) {

		if (validity != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			String format = formatter.format(validity);
			return format;
		} else {
			return null;
		}
	}

	/**
	 * Method to convert license data from overview business object
	 * @param licenseOverview: Object
	 * @param listEnv: List of Environment
	 * @param listCust: List of Customer
	 * @return: Object
	 */
	public License convertLicenseData(LicenseOverviewBO licenseOverview, List<Environment> listEnv, List<Customer> listCust) {
		LOGGER.log(Level.INFO, "CustomerRestServiceUtil.convertLicenseData --- Inside -----");
		License license = new License();
		license.setCurrency(licenseOverview.getCurrency());
		
		Customer savecust = new Customer();
		for(Customer customer: listCust){
			if (customer.getName().equalsIgnoreCase(licenseOverview.getCustomerName())){
				savecust.setCustomerId(customer.getCustomerId());
			}
		}
		savecust.setName(licenseOverview.getCustomerName());
		license.setCustomerObject(savecust);
		
		Environment saveEnv = new Environment();
		for(Environment env: listEnv){
			if (env.getName().equalsIgnoreCase(licenseOverview.getEnvironmentName())){
				saveEnv.setEnvId(env.getEnvId());
			}
		}
		saveEnv.setName(licenseOverview.getEnvironmentName());
		license.setEnvironmentObject(saveEnv);
		
		license.setPrice(Double.valueOf(licenseOverview.getPrice()));
		
		license.setValidity(convertDateString(licenseOverview.getValidity()));
		
		license.setLicenseId(licenseOverview.getLicenseId());
		
		license.setListOfLicensePermission(licenseOverview.getListOfPermission());
		
		for(LicensePermission licensePermissions: license.getListOfLicensePermission()) {
			licensePermissions.setLicenseObject(license);
		}
		LOGGER.log(Level.INFO, "CustomerRestServiceUtil.convertLicenseData --- Exit -----");
		return license;
	}	
	
	private Date convertDateString(String strDate) {

		if(null != strDate) {
	//	SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		Date validDate = null;
		try {
		//	Date tempDate = inputFormat.parse(strDate);
		//	String date = sdf.format(tempDate);
			validDate = sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return validDate;
		}else {
			return null;
		}
	}

}
