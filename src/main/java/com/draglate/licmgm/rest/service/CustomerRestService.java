package com.draglate.licmgm.rest.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.draglate.licmgm.entity.Customer;
import com.draglate.licmgm.entity.Environment;
import com.draglate.licmgm.entity.License;
import com.draglate.licmgm.entity.LicensePermission;
import com.draglate.licmgm.entity.Permission;
import com.draglate.licmgm.BO.license.LicenseOverviewBO;
import com.draglate.licmgm.rest.util.CustomerRestServiceUtil;

/**
 * Service Class for License Management
 * @author Abhijit Ingle
 *
 */
@Path("/customer")
public class CustomerRestService {
	private final static Logger LOGGER = Logger.getLogger(CustomerRestService.class.getName());
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("license_mgm_unit");
	EntityManager em = emf.createEntityManager();
	
	/**
	 * Method fetch all license and their customer details
	 * @return List of Customers and License details
	 */
    @SuppressWarnings("unchecked")
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<LicenseOverviewBO> getCustomerEditBO() {
    	LOGGER.log(Level.INFO, "CustomerRestService.getCustomerEditBO --- Inside -----");
    	 List<LicenseOverviewBO> listOfLicenseOverview = new ArrayList<LicenseOverviewBO>();
       
    	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 } 
    	 List<License> listOfLicense= em.createQuery("SELECT licnse FROM License licnse").getResultList();
    	 CustomerRestServiceUtil customerRestServiceUtil = new CustomerRestServiceUtil();
    	
    	 
    	 for (License licenseObject:listOfLicense) {
    		 LicenseOverviewBO overviewBO = new LicenseOverviewBO();
    		 
    		 customerRestServiceUtil.getLicenseOverviewBO(overviewBO, licenseObject);   		 
    		 
    		 listOfLicenseOverview.add(overviewBO);
    		 LOGGER.log(Level.INFO, listOfLicenseOverview.toString());
    	 }
    	 LOGGER.log(Level.INFO, "CustomerRestService.getCustomerEditBO --- Exit -----");
    	return listOfLicenseOverview;
    }
    
    /**
     * Delete the license record
     * @param licenseIdString- license id to be deleted
     * @return transaction status
     */
    @GET
	@Path("{licenseId}")
	public int deleteLicenseRecord(@PathParam("licenseId") String licenseIdString) {
    	if(!em.getTransaction().isActive()){
   		 em.getTransaction().begin();
   	 	} 

    	LOGGER.log(Level.INFO, "CustomerRestService.deleteLicenseRecord --- Inside -----");
    	
		License license = findLicenseById(licenseIdString);

		Query query = em.createQuery(
				"DELETE FROM LicensePermission licPermission where licPermission.licenseObject =:licenseobject");
		query.setParameter("licenseobject", license).executeUpdate();

		Query queryLicense = em.createQuery("DELETE FROM License licn where licn.licenseId =:id");
		int deletedCount = queryLicense.setParameter("id", Long.valueOf(licenseIdString)).executeUpdate();
		
		em.getTransaction().commit();
		LOGGER.log(Level.INFO, "CustomerRestService.deleteLicenseRecord --- Exit -----");

		return deletedCount;

	}
    
    /**
     * Save new license and update old license
     * @param licenseObject: License data to be saved
     * @return status of transaction
     */
    @GET
	@Path("{licenseObject}/save")  
	public Long saveLicense(@PathParam("licenseObject") String licenseObject) {
    	LOGGER.log(Level.INFO, "CustomerRestService.saveLicense --- Inside -----");
    	LicenseOverviewBO licenseOverview=null;
    	License license = null;
		try {
			licenseOverview = new ObjectMapper().readValue(licenseObject,LicenseOverviewBO.class);
			
			CustomerRestServiceUtil customerRestServiceUtil = new CustomerRestServiceUtil();
			
			license = customerRestServiceUtil.convertLicenseData(licenseOverview,getListOfEnvironment(),getListOfCustomer());
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
    	
		if(!em.getTransaction().isActive()){
   		 em.getTransaction().begin();
   	 	} 

		em.merge(license);
	
		em.flush();
		
		em.getTransaction().commit();
		
		LOGGER.log(Level.INFO, "CustomerRestService.saveLicense --- Exit -----");
		
		return license.getLicenseId();
		
	}


    /**
     * Return List of Permission
     * @return: List of Permission
     */
	@SuppressWarnings("unchecked")
	@GET
	@Path("listOfpermission/listp")  
    @Produces(MediaType.APPLICATION_JSON)
    public List<Permission> getListOfPermission() {
		 LOGGER.log(Level.INFO, "CustomerRestService.getListOfPermission --- Inside -----");
    	 List<Permission> listOfPermission = new ArrayList<Permission>();
       
    	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 } 
    	 listOfPermission = em.createQuery("SELECT permission FROM Permission permission").getResultList();
    	 LOGGER.log(Level.INFO, "CustomerRestService.getListOfPermission --- Exit -----");    	
    	 return listOfPermission;
    }
	
	/**
	 * Return list of license permission
	 * @return- list of permission related to license
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Path("listOfpermission/listlp")  
    @Produces(MediaType.APPLICATION_JSON)
    public List<LicensePermission> getListOfLicensePermission() {
		LOGGER.log(Level.INFO, "CustomerRestService.getListOfLicensePermission --- Inside -----");   	
    	 List<LicensePermission> listOfLicensePermission = new ArrayList<LicensePermission>();
       
    	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 } 
    	 listOfLicensePermission = em.createQuery("SELECT lPermission FROM LicensePermission lPermission").getResultList();
    	 LOGGER.log(Level.INFO, "CustomerRestService.getListOfLicensePermission --- Exit -----");
     		
    	 return listOfLicensePermission;
    }
	
	/**
	 * Return list of Environment
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Path("listOfEnv")  
    @Produces(MediaType.APPLICATION_JSON)
    public List<Environment> getListOfEnvironment() {
    	 List<Environment> listOfEnvironment = new ArrayList<Environment>();
        
     	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 }
    	 
    	 listOfEnvironment = em.createQuery("SELECT envmnt FROM Environment envmnt").getResultList();
    	    	   	
    	return listOfEnvironment;
    }
	
	/**
	 * Return list of customers
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Path("listOfCust")  
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> getListOfCustomer() {
    	 List<Customer> listOfCustomer = new ArrayList<Customer>();
       
    	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 }            	 
    	 listOfCustomer = em.createQuery("SELECT cust FROM Customer cust").getResultList();
     	   	
    	return listOfCustomer;
    }
    
    /**
     * Method to find the License info based on license id
     * @param licenseIdString: License Id
     * @return: Object og License
     */
	public License findLicenseById(String licenseIdString) {
		
		if(!em.getTransaction().isActive()){
   		 em.getTransaction().begin();
   	 	} 
		
		Query query= em.createQuery("SELECT licnse FROM License licnse where licnse.licenseId =:id");
   	 	query.setParameter("id", Long.valueOf(licenseIdString));
   	 	
   	 	License license = (License) query.getSingleResult();
   	 	
		return license;
	}
	

	@SuppressWarnings("unchecked")
	@GET
	@Path("license/permissionList/all")  
    @Produces(MediaType.APPLICATION_JSON)
    public List<LicensePermission> getListOfLicPermission() {
		LOGGER.log(Level.INFO, "CustomerRestService.getListOfLicPermission --- Inside -----");   
		List<LicensePermission> listOfLicensePermission = new ArrayList<LicensePermission>();
       
    	 if(!em.getTransaction().isActive()){
    		 em.getTransaction().begin();
    	 } 
    	 listOfLicensePermission = em.createQuery("SELECT lPermission FROM LicensePermission lPermission").getResultList();
    	 LOGGER.log(Level.INFO, "CustomerRestService.getListOfLicPermission --- Exit -----");  	
    	 return listOfLicensePermission;
    }

    /**
     * Save permission
     * @param permissionName: Object to save
     * @return
     */
    @GET
	@Path("{permissionName}/permission/obj")
    @Produces(MediaType.APPLICATION_JSON)
	public Long savePermission(@PathParam("permissionName") String permissionName) {
    	
    	LOGGER.log(Level.INFO, "CustomerRestService.savePermission --- Inside -----");
    	Permission permission = new Permission();
    	permission.setName(permissionName);
    	
    	if(!em.getTransaction().isActive()){
      		 em.getTransaction().begin();
      	 	} 

   		em.persist(permission);
   	
   		em.flush();
   		
   		em.getTransaction().commit();
   		LOGGER.log(Level.INFO, "CustomerRestService.savePermission --- Exit -----");
   		return permission.getPermissionId();
    }
    
	 /**
	  * Save new license Permission
	  * @param licensePermList Permission Object
	  */
    @GET
	@Path("{licensePermList}/save/licensepermission/list")  
	public void saveLicensePermission(@PathParam("licensePermList") String licensePermList) {
    	
    	LOGGER.log(Level.INFO, "CustomerRestService.saveLicensePermission --- Inside -----");
    	LicensePermission permission = new LicensePermission();
    	
    	if(!em.getTransaction().isActive()){
      		 em.getTransaction().begin();
      	 	} 

   		em.merge(permission);
   	
   		em.flush();
   		
   		em.getTransaction().commit();
   		LOGGER.log(Level.INFO, "CustomerRestService.saveLicensePermission --- Exit -----");
    }

}
