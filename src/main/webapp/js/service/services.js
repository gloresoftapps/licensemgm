// service



licenseApp.factory('LicenseFactory', function ($resource) {
	
	return $resource('/license_mgm/rest/customer',{
	      query: {method: 'GET',
	    	  transformResponse: function (data) {return angular.fromJson(data).LicenseFactory}
	    	  ,isArray: true}})
});

licenseApp.factory('DeleteLicence', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/:licenseId', {licenseId:'@licenseId'});
		
});


licenseApp.factory('SaveLicense', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/:licenseData/save', {licenseData:'@licenseData'});
		
});

licenseApp.factory('ListOfPermission', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/listOfpermission/listp');
		
});

licenseApp.factory('ListOfLicensePermission', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/listOfpermission/listlp');
		
});


licenseApp.factory('ListOfEnv', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/listOfEnv');
		
});

licenseApp.factory('ListOfCustomer', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/listOfCust');
		
});

licenseApp.factory('ListOfLicPermissionObject', function ($resource) {
	
	return $resource('/license_mgm/rest/customer/license/permissionList/all',{
	      query: {method: 'GET',
	    	  transformResponse: function (data) {return angular.fromJson(data).ListOfLicPermissionObject}
	    	  ,isArray: true}})
		
});


licenseApp.factory('SavePermissionObject', function ($resource) {	
	return $resource('/license_mgm/rest/customer/:permissionName/permission/obj', {permissionName:'@permissionName'});		
});


licenseApp.factory('SaveLicensePermission', function ($resource) {	
	return $resource('/license_mgm/rest/customer/:licensePermList/save/licensepermission/list', {permissionName:'@permissionName'});		
});



