// routes

licenseApp.config(function($routeProvider) {
	
	$routeProvider
	
	.when('/home', {
		
		templateUrl: 'index.html',
		controller: 'overviewController'
	  
		
	})
	
	.when('/customeredit', {
		
		templateUrl: 'pages/customerEdit',
		controller: 'customerController'
		
	})
	
	
});
