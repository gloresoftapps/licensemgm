licenseApp.controller('overviewController', ['$scope', 'LicenseFactory','DeleteLicence', 
                                             'ListOfPermission','ListOfLicensePermission',function ($scope, LicenseFactory,deleteLicense,
                                              listOfPermission,listOfLicPermission) {
	
		$scope.listOflicense = LicenseFactory.query();
		
		$scope.listOfPermission = angular.fromJson(LicenseFactory.query());
		
		$scope.licenseId = 0;
		
		$scope.listOfPermissionObject = listOfPermission.query();
		
		$scope.listOfLicencePermissionObject = listOfLicPermission.query();
		
		
        $scope.setLicenseId = function (licenId) { 
        	$scope.licenseId = licenId;
        };

       
        $scope.deleteLicenseId = function () {
        	deleteLicense.get( {licenseId:$scope.licenseId}, function(deleteLicence){
        		$scope.listOflicense = LicenseFactory.query();
        		$scope.showSuccessAlert = true;
        	});
        };
        
        
        $scope.fetchValue = function(permissionId, licenseObjectID) {
        	var flag = 1;
        	for(var object in $scope.listOfLicencePermissionObject){
        		if($scope.listOfLicencePermissionObject[object].licenseId
        				== licenseObjectID && $scope.listOfLicencePermissionObject[object].permissionObject.permissionId == permissionId){
        			flag = 0;
        			return $scope.listOfLicencePermissionObject[object].value;
        		}       		
        	}       	
        	if(flag==1){
        		return "";
        	}
       	
        };
        
}]);
