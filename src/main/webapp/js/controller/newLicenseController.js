//// controller

licenseApp.controller('customerController', ['$scope','$routeParams','SaveLicense','ListOfEnv','ListOfCustomer','$location','ListOfPermission','$filter',
                                             'ListOfLicPermissionObject','ListOfLicensePermission','SavePermissionObject','SaveLicensePermission',
                                             function ($scope,$routeParams,saveLicense,listOfEnv,listOfCust,$location,listOfPermission,$filter,
                                            listOfLicPermissionObject,listOfLicPermission,savePermissionObject,saveLicensePermission) {
	
	     // $scope.listOfLicPermsn = angular.fromJson(listOfLicPermissionObject.query());
          	 
     	 // $scope.listOfLicPermsn = listOfLicPermissionObject.query();
		 $scope.listOfTempPermission = new Array();
		 
		 $scope.cnt = 0;
     	 	
		 $scope.environments = listOfEnv.query();
	    
	     $scope.customers = listOfCust.query();
	
 		 $scope.licenseObject = angular.fromJson($routeParams.licObj);
 		 
 		 $scope.licensejson = $routeParams.licObj;
 		 
 		 $scope.listOfPermissionObject = listOfPermission.query();
 		 		 
 		 $scope.save = function (licenseObj) {
 			 
	 			
	 			  licenseObj.listOfPermission = new Array();
	 			
	 			 			 
	 			 for(k in $scope.listOfTempPermission){      	      
	 			 
	 				 var permissionObject = {name : $scope.listOfTempPermission[k].name, permissionId: $scope.listOfTempPermission[k].id};
	 			
	 				 var data = {permissionObject : permissionObject, value : $scope.listOfTempPermission[k].value};
	 				 
	 				licenseObj.listOfPermission.push(data);
	     	    
	 			 } 
	 			 
	 			 $scope.licensejson = angular.toJson(licenseObj);
	 			 
		 		 saveLicense.get( {licenseData: $scope.licensejson}, function(saveLicense){
			 			 $scope.showSuccessUpdate = true;
			 			 $scope.go('/overview');
		         });	
 			
         };
                
         $scope.go = function(path) {
        	  $location.path(path);
         };
         
//   Date Picker UI-Bootstrap         
//         $scope.open1 = function() {
//        	    $scope.popup1.opened = true;
//         };
//         $scope.popup1 = {
//        	    opened: false
//         };
           
//           Date Conversion           
//           $scope.converDate = function(licenseDate) {
//        	   if(null != licenseDate) {
//           	   	 return $filter('date')( licenseDate,'fullDate');          		   
//        	   } 
//           }
           
           
           
           $scope.savePermission  = function(permissionName, permissionValue) {
        	   
        	   var permissionId = null;
        	   
        	   var permissionChar = angular.toJson(permissionName);
        	   
        	   if(permissionChar.includes('name')){
        		   permissionName = permissionName.name;
        	   }
        	   
        	   for(k in $scope.listOfTempPermission){
        		   if($scope.listOfTempPermission[k].name == permissionName) {
        			   $scope.showSuccessError = true;
        			   return;
        		   }
        	   }
        	   
        	   for(var j in $scope.listOfPermissionObject){
        		   if($scope.listOfPermissionObject[j].name == permissionName){
        			   permissionId = $scope.listOfPermissionObject[j].permissionId;
        		   }
        	   }
        	           	         	   
//        	   if(permissionId == null) {
//        		   var tempData1 = savePermissionObject.get( {permissionName: permissionName}, function(savePermissionObject){              		
//                	});	
//        		   
//        		   var temp = tempData1.$promise.then(function(repsonse){
//               		var tempData = angular.toJson(repsonse);
//               		alert(angular.fromJson(tempData));
//               		permissionId = tempData.data;
//               	})
//               	
//               	alert (temp)
//        	   }
        	   
        	   var data = {name : permissionName, value : permissionValue, id: permissionId};
        	   $scope.listOfTempPermission.push(data);
        	   $scope.cnt =  $scope.cnt + 1;
        	   $scope.permissionValue = null;
        	   
        	         	   
           }
           
           
           $scope.getPermissionList = function(){
        	  if($scope.licenseObject.listOfPermission != undefined) {
        	   for(z in $scope.licenseObject.listOfPermission) {
        		   var data = {name : $scope.licenseObject.listOfPermission[z].permissionObject.name, value : $scope.licenseObject.listOfPermission[z].value, id: $scope.licenseObject.listOfPermission[z].permissionObject.permissionId};
            	   $scope.listOfTempPermission.push(data);
            	   $scope.cnt =  $scope.cnt + 1;
        	   }
        	  }
        	
        	   return $scope.listOfTempPermission;
        	  
           }
           
           $scope.getCount  = function() {
        	   return $scope.cnt;
           }
           
}]);

