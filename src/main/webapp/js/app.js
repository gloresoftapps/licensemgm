// module
var licenseApp = angular.module('licenseApp', ['ngRoute', 'ngResource', 'ngMessages', 'ngAnimate', 'ngSanitize', 'ui.bootstrap']);


// routes

licenseApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	
	
	$routeProvider
	
	.when('/customer', {
		
		templateUrl: 'pages/customerEdit.html',
		controller: 'customerController'
	  
		
	})
	
	.when('/license/:licObj', {
		
		templateUrl: 'pages/customerEdit.html',
		controller: 'customerController'
	  
		
	})
	
	.when('/overview', {
		
		templateUrl: 'pages/overview.html',
		controller: 'overviewController'
		
	})
	
	.when('/', {
		
		templateUrl: 'pages/overview.html',
		controller: 'overviewController'
		
	})
	
	
}]);

// service

//weatherApp.service('cityService',function(){
//	
//	this.cityName = 'Pune, PN';
//	
//	
//});

// controller

//licenseApp.controller('overviewController',['$scope', 'cityService', function($scope, cityService){
//	
//	$scope.cityName = cityService.cityName;
//	
//	$scope.$watch('cityName', function() {
//		cityService.cityName = $scope.cityName;
//	});
//	
//}]);



//weatherApp.controller('forecastController',['$scope', '$resource', '$routeParams', 'cityService', function($scope, $resource, $routeParams, cityService){
//	
//	$scope.cityName = cityService.cityName;
//	
//	$scope.days = $routeParams.days || 4;
//	
//	$scope.weatherApi = $resource('http://api.openweathermap.org/data/2.5/forecast', {
//	callback: "JSON_CALLBACK" }, {get: {method: "JSONP"}});
//	
//	$scope.weatherResult = $scope.weatherApi.get({q:$scope.cityName, cnt: $scope.days, appid:'1afa9504b8ca8b3fb1e30875568aeaaa' });
//	
//	console.log($scope.weatherResult);
//	
//	$scope.convertFehren = function (degk) {
//		return Math.round((1.8 * (degk - 273)) + 32);
//	}
//		
//}]);