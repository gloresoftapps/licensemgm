package com.draglate.licmgm.rest.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.draglate.licmgm.BO.license.LicenseOverviewBO;
import com.draglate.licmgm.entity.Customer;
import com.draglate.licmgm.entity.Environment;
import com.draglate.licmgm.entity.License;

public class CustomerRestServiceutilTest {
	
	private LicenseOverviewBO overviewBO ;
	
	private License license;

	@Before
	public void setUp() throws Exception {
		 overviewBO = new LicenseOverviewBO();
		 overviewBO.setPrice("400");
		 license = new License();
		 license.setCustomerObject(new Customer());
		 license.setEnvironmentObject(new Environment());
		 license.setPrice(new Double("400"));
	}

	@Test
	public void getLicenseOverviewBOTest() {
		
		CustomerRestServiceUtil  customerRestServiceUtil = new CustomerRestServiceUtil();
		customerRestServiceUtil.getLicenseOverviewBO(overviewBO, license);
		assertEquals(overviewBO.getPrice(), license.getPrice().toString());
	}

}
